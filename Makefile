#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/05 16:12:14 by cpestour          #+#    #+#              #
#    Updated: 2015/04/28 13:34:17 by cpestour         ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=fdf
CFLAGS=-Wall -Werror -Wextra -g -I/usr/include -Iincludes -Ilibft/includes
LDFLAGS=-Llibft -lft -lmlx -framework OpenGL -framework AppKit
SRC=srcs/main.c srcs/fdf.c srcs/parse.c srcs/draw.c srcs/math.c srcs/zoom.c srcs/move.c srcs/exit.c
OBJ=$(SRC:.c=.o)

all: lib $(NAME)

lib:
	@make -C libft

$(NAME): $(OBJ)
	@gcc -o $@ $^ $(LDFLAGS)
	@echo "\033[33;32mfdf done\033[33;37m"

%.o: %.c
	@gcc -o $@ -c $< $(CFLAGS)

clean:
	@rm -f *~ $(OBJ) srcs/*~ includes/*~
	@echo "\033[33;31mclean done\033[33;37m"

fclean: clean
	@make fclean -C libft
	@rm -f $(NAME)
	@echo "\033[33;31mfclean done\033[33;37m"

re: fclean all
