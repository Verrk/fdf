/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/05 16:19:11 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/14 16:13:38 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <stdlib.h>
# include <mlx.h>
# include <string.h>
# include <libft.h>
# include <stdio.h>

# define WIN_X	1280
# define WIN_Y	720
# define SAND	0xFFB255
# define BROWN	0x8B4513
# define GREEN	0x008000
# define BLUE	0x0000FF
# define SNOW	0x99CCFF

typedef struct		s_point
{
	int				d3x;
	int				d3y;
	int				d3z;
	int				d2x;
	int				d2y;
}					t_point;

typedef struct		s_bres
{
	int				dx;
	int				dy;
	int				sx;
	int				sy;
	int				err;
}					t_bres;

typedef struct		s_fdf
{
	void			*mlx;
	void			*win;
	void			*img;
	char			*data;
	int				bpp;
	int				s_line;
	int				end;
	t_point			**map;
	int				max_x;
	int				max_y;
	int				max_z;
	float			pad_x;
	float			pad_y;
	int				move_x;
	int				move_y;
	char			*path;
}					t_fdf;

int					ft_fdf(t_fdf *fdf);
t_point				**get_map(t_fdf *fdf);
void				get_coord_map(t_fdf *fdf);
void				draw_map(t_fdf *fdf);
void				reload(t_fdf *fdf);
void				zoom_in(t_fdf *fdf);
void				zoom_out(t_fdf *fdf);
void				move_right(t_fdf *fdf);
void				move_left(t_fdf *fdf);
void				move_up(t_fdf *fdf);
void				move_down(t_fdf *fdf);
void				ft_exit(t_fdf *fdf);

#endif
