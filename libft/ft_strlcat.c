/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 15:06:33 by cpestour          #+#    #+#             */
/*   Updated: 2014/10/07 08:32:48 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t	dst_len;
	size_t	src_len;

	dst_len = ft_strlen(dst);
	size -= dst_len + 1;
	if (!size)
		return (dst_len);
	src_len = ft_strlen(src);
	if (src_len > size)
		src_len = size;
	ft_memcpy(dst + dst_len, src, src_len);
	dst[dst_len + src_len] = '\0';
	return (dst_len + src_len);
}
