/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit_space.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/01/05 15:35:40 by cpestour          #+#    #+#             */
/*   Updated: 2015/02/08 15:58:24 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

static int	calcword(const char *s)
{
	int	i;
	int	word;

	i = 0;
	word = 0;
	while (s[i])
	{
		if ((!ft_isblank(s[i]) && i == 0) ||
			(!ft_isblank(s[i]) && ft_isblank(s[i - 1])))
			word++;
		i++;
	}
	return (word);
}

static char	**malloctab(char const *s)
{
	int	numword;

	if (s)
	{
		numword = calcword(s);
		return ((char **)ft_memalloc(sizeof(char *) * (numword + 1)));
	}
	return (NULL);
}

static int	calcmalloc(char const *s, int i)
{
	int	n;

	n = 0;
	while (s[i] && !ft_isblank(s[i]))
	{
		n++;
		i++;
	}
	return (n);
}

char		**ft_strsplit_space(char const *s)
{
	char	**tab;
	int		i;
	int		n;
	int		j;

	if (s == NULL || (tab = malloctab(s)) == NULL)
		return (NULL);
	i = -1;
	n = 0;
	while (s[++i])
	{
		if (!ft_isblank(s[i]))
		{
			if (tab[n] == NULL)
			{
				j = 0;
				if ((tab[n] = ft_strnew(calcmalloc(s, i))) == NULL)
					return (NULL);
			}
			tab[n][j++] = s[i];
		}
		else if (tab[n] != NULL)
			n++;
	}
	return (tab);
}
