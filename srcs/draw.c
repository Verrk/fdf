/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 23:57:32 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/27 15:31:48 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void		draw_point(t_fdf *fdf, int x, int y, int color)
{
	if (x > 0 && x < WIN_X && y > 0 && y < WIN_Y)
		ft_memcpy(&fdf->data[x * 4 + y * fdf->s_line], &color, 4);
}

static t_bres	get_bres(t_point p1, t_point p2)
{
	t_bres		bres;

	bres.dx = ft_abs(p2.d2x - p1.d2x);
	bres.dy = ft_abs(p2.d2y - p1.d2y);
	bres.sx = p1.d2x < p2.d2x ? 1 : -1;
	bres.sy = p1.d2y < p2.d2y ? 1 : -1;
	bres.err = bres.dx > bres.dy ? bres.dx / 2 : -bres.dy / 2;
	return (bres);
}

static int		get_color(int z1, int z2, int max)
{
	if ((z1 + z2) / 2 <= 0)
		return (BLUE);
	else if ((z1 + z2) / 2 > max * 0.7)
		return (SNOW);
	else if ((z1 + z2) / 2 > max * 0.5)
		return (BROWN);
	else if ((z1 + z2) / 2 < max * 0.1)
		return (SAND);
	else
		return (GREEN);
}

static void		draw_line(t_fdf *fdf, t_point point1, t_point point2)
{
	t_bres		bres;
	int			e;
	int			x;
	int			y;

	x = point1.d2x;
	y = point1.d2y;
	bres = get_bres(point1, point2);
	while (42)
	{
		draw_point(fdf, x, y, get_color(point1.d3z, point2.d3z, fdf->max_z));
		if (x == point2.d2x && y == point2.d2y)
			break ;
		e = bres.err;
		if (e > -bres.dx)
		{
			bres.err -= bres.dy;
			x += bres.sx;
		}
		if (e < bres.dy)
		{
			bres.err += bres.dx;
			y += bres.sy;
		}
	}
}

void			draw_map(t_fdf *fdf)
{
	int			i;
	int			j;

	i = 0;
	while (i < fdf->max_y)
	{
		j = 0;
		while (j < fdf->max_x)
		{
			if (j < fdf->max_x - 1)
				draw_line(fdf, fdf->map[i][j], fdf->map[i][j + 1]);
			if (i < fdf->max_y - 1)
				draw_line(fdf, fdf->map[i][j], fdf->map[i + 1][j]);
			j++;
		}
		i++;
	}
}
