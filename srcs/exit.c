/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/28 13:13:30 by cpestour          #+#    #+#             */
/*   Updated: 2015/04/28 13:14:24 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		ft_exit(t_fdf *fdf)
{
	int		i;

	i = 0;
	while (fdf->map[i])
	{
		free(fdf->map[i]);
		i++;
	}
	free(fdf->map);
	free(fdf->path);
	mlx_destroy_window(fdf->mlx, fdf->win);
	free(fdf->mlx);
	exit(0);
}
