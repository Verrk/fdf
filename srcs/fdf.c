/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 23:53:22 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/27 15:39:35 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	expose_hook(t_fdf *fdf)
{
	get_coord_map(fdf);
	draw_map(fdf);
	mlx_put_image_to_window(fdf->mlx, fdf->win, fdf->img, 0, 0);
	return (0);
}

void		reload(t_fdf *fdf)
{
	ft_bzero(fdf->data, WIN_X * WIN_Y * 4);
	expose_hook(fdf);
}

static int	key_hook(int keycode, t_fdf *fdf)
{
	if (keycode == 53)
		ft_exit(fdf);
	if (keycode == 69)
		zoom_in(fdf);
	if (keycode == 78)
		zoom_out(fdf);
	if (keycode == 126)
		move_up(fdf);
	if (keycode == 125)
		move_down(fdf);
	if (keycode == 123)
		move_left(fdf);
	if (keycode == 124)
		move_right(fdf);
	return (0);
}

static int	mouse_hook(int button, int x, int y, t_fdf *fdf)
{
	(void)x;
	(void)y;
	if (button == 5)
	{
		if (fdf->pad_y > 0.1)
		{
			fdf->pad_y -= 0.1;
			reload(fdf);
		}
	}
	if (button == 4)
	{
		fdf->pad_y += 0.1;
		reload(fdf);
	}
	return (0);
}

int			ft_fdf(t_fdf *fdf)
{
	fdf->mlx = mlx_init();
	fdf->win = mlx_new_window(fdf->mlx, WIN_X, WIN_Y, "fdf");
	fdf->img = mlx_new_image(fdf->mlx, WIN_X, WIN_Y);
	fdf->data = mlx_get_data_addr(fdf->img, &fdf->bpp, &fdf->s_line, &fdf->end);
	fdf->map = get_map(fdf);
	fdf->pad_x = 516 / fdf->max_x;
	if (fdf->max_z)
		fdf->pad_y = (float)((float)50 / (float)fdf->max_z);
	mlx_expose_hook(fdf->win, expose_hook, fdf);
	mlx_key_hook(fdf->win, key_hook, fdf);
	mlx_mouse_hook(fdf->win, mouse_hook, fdf);
	mlx_loop(fdf->mlx);
	return (0);
}
