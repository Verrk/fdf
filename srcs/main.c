/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/05 16:18:40 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/27 15:36:34 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		init(t_fdf *fdf)
{
	fdf->max_x = 0;
	fdf->max_y = 0;
	fdf->max_z = 0;
	fdf->pad_x = 1;
	fdf->pad_y = 0.1;
	fdf->move_x = 0;
	fdf->move_y = 0;
}

int			main(int ac, char **av)
{
	t_fdf	fdf;
	int		fd;

	if (ac == 2)
	{
		if ((fd = open(av[1], O_RDONLY)) == -1)
		{
			close(fd);
			ft_putstr_fd("Invalid file\n", 2);
		}
		else
		{
			close(fd);
			init(&fdf);
			fdf.path = ft_strdup(av[1]);
			ft_fdf(&fdf);
		}
	}
	return (0);
}
