/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   math.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/11 00:00:29 by cpestour          #+#    #+#             */
/*   Updated: 2015/03/15 04:54:49 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	get_coord_point(t_fdf *fdf, t_point *p)
{
	int		x;
	int		y;
	int		z;

	x = p->d3x * fdf->pad_x;
	y = p->d3y * fdf->pad_x;
	z = p->d3z * fdf->pad_y;
	p->d2x = x - y + WIN_X * 0.5 + fdf->move_x;
	p->d2y = -z + x * 0.5 + y * 0.5 + WIN_Y * 0.2 + fdf->move_y;
}

void		get_coord_map(t_fdf *fdf)
{
	int		i;
	int		j;

	i = 0;
	while (i < fdf->max_y)
	{
		j = 0;
		while (j < fdf->max_x)
		{
			get_coord_point(fdf, &(fdf->map[i][j]));
			j++;
		}
		i++;
	}
}
