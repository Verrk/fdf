/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/11 00:10:21 by cpestour          #+#    #+#             */
/*   Updated: 2015/02/11 00:12:08 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		move_right(t_fdf *fdf)
{
	fdf->move_x += 10;
	reload(fdf);
}

void		move_left(t_fdf *fdf)
{
	fdf->move_x -= 10;
	reload(fdf);
}

void		move_up(t_fdf *fdf)
{
	fdf->move_y -= 10;
	reload(fdf);
}

void		move_down(t_fdf *fdf)
{
	fdf->move_y += 10;
	reload(fdf);
}
