/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: cpestour <cpestour@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/10 23:55:29 by cpestour          #+#    #+#             */
/*   Updated: 2015/05/27 14:08:17 by cpestour         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void			check_len(char *buf, int fd)
{
	if (ft_strlen(buf) >= 7144)
	{
		ft_putendl("Map too big");
		free(buf);
		close(fd);
		exit(1);
	}
}

static int		len_map(t_fdf *fdf)
{
	int			fd;
	char		*buff;
	int			size;
	char		**tab;
	int			i;

	fd = open(fdf->path, O_RDONLY);
	size = 0;
	while (get_next_line(fd, &buff))
	{
		check_len(buff, fd);
		size++;
		tab = ft_strsplit_space(buff);
		fdf->max_x = ft_strlen_tab(tab) > fdf->max_x ?
			ft_strlen_tab(tab) : fdf->max_x;
		i = -1;
		while (tab[++i])
			fdf->max_z = ft_atoi(tab[i]) > fdf->max_z ?
				ft_atoi(tab[i]) : fdf->max_z;
		ft_tabdel(tab);
		free(buff);
	}
	fdf->max_y = size;
	close(fd);
	return (size);
}

static t_point	get_map_point(char *h, int i, int j)
{
	t_point		p;

	p.d3x = j;
	p.d3y = i;
	p.d3z = ft_atoi(h);
	return (p);
}

static t_point	*get_map_line(t_fdf *fdf, char *line, int i)
{
	t_point		*map_line;
	int			j;
	char		**tab;

	tab = ft_strsplit_space(line);
	map_line = (t_point *)malloc(sizeof(t_point) * (fdf->max_x + 1));
	j = 0;
	while (tab[j])
	{
		map_line[j] = get_map_point(tab[j], i, j);
		j++;
	}
	while (j < fdf->max_x)
	{
		map_line[j] = get_map_point("-1", i, j);
		j++;
	}
	ft_tabdel(tab);
	return (map_line);
}

t_point			**get_map(t_fdf *fdf)
{
	t_point		**map;
	int			fd;
	int			i;
	char		*line;

	map = (t_point **)malloc(sizeof(t_point *) * (len_map(fdf) + 1));
	fd = open(fdf->path, O_RDONLY);
	i = 0;
	while (get_next_line(fd, &line))
	{
		map[i] = get_map_line(fdf, line, i);
		free(line);
		i++;
	}
	map[i] = NULL;
	close(fd);
	return (map);
}
